package com.fsck.k9.controller

import android.content.Context
import android.media.AudioManager
import android.media.MediaPlayer
import com.fsck.k9.core.R

object MessageSentAudio {
    @JvmStatic
    fun play(context: Context) {
        val mediaPlayer = MediaPlayer.create(context, R.raw.email_sent)
        mediaPlayer.setOnCompletionListener { it.release() }

        // set volume to match notification volume settings
        val audioManager = context.getSystemService(Context.AUDIO_SERVICE) as AudioManager?
        val maxVolume = audioManager?.getStreamMaxVolume(AudioManager.STREAM_NOTIFICATION) ?: 1
        val currentVolume = audioManager?.getStreamVolume(AudioManager.STREAM_NOTIFICATION) ?: 0
        mediaPlayer.setVolume((currentVolume / maxVolume.toFloat()), (currentVolume / maxVolume.toFloat()))

        mediaPlayer.start()
    }
}
