package com.fsck.k9.mail.oauth.authorizationserver.codegrantflow

import com.fsck.k9.mail.MessagingException

class OAuth2NeedUserPromptException : MessagingException("Need user's prompt for xoauth2")
